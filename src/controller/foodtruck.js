import mongoose from 'mongoose';
import router from 'express';
import FoodTruck from '../model/foodtruck';
import Review from '../model/review';

import bodyParser from 'body-parser';

import {authenticate} from '../middleware/authMiddleware';

export default ({ config, db }) => {
  let api = router();


//'v1/foodtruck/add'
// adding authenticate here means we have to include
// the Bearer token in post requests.
  api.post('/add', authenticate, (req, res) => {

    let newTruck = new FoodTruck();
    newTruck.name = req.body.name;
    newTruck.foodtype = req.body.foodtype;
    newTruck.avgcost = req.body.avgcost;
    newTruck.geometry.coordinates = req.body.geometry.coordinates;

    newTruck.save(err => {
      if (err) {
        res.send(err);
      }
      res.json({message: 'Foodtruck saved successfully'});
    });
  });

  // read
  // 'v1/foodtruck'

  api.get('/',(req, res) => {
    Foodtruck.find({}, (err, foodtrucks) => {
      if (err) {
      res.send(err);
    }
    res.json(foodtrucks);
  });
});

// 'v1/foodtruck/:id' -Read 1
api.get('/:id',(req, res) => {
  Foodtruck.findById(req.params.id, (err, foodtruck) => {
    if (err) {
    res.send(err);
  }
  res.json(foodtruck);
});
});

//'v1/foodtruck:id' -update
    api.put('/:id', (req, res) => {
      Foodtruck.findById(req.params.id, (err, foodtruck) => {
        if (err) {
            res.send(err);
        }
      foodtruck.name = req.body.name;
      foodtruck.save(err => {
        if (err) {
          res.send(err);
        }
      res.json({message: "foodtruck info updated"});
    });
  });
});

// 'v1/restaurant:id' - Delete
api.delete('/:id', (req, res) => {
  Foodtruck.remove({
    _id: req.params.id
  }, (err, foodtruck) => {
    if (err) {
      res.send(err);
    }
    res.json({message: "foodtruck deleted"});
  });
});

// add review

//'/v1/foodtruck/reviews/add/:id'
api.post('/reviews/add/:id', (req, res)=>{
  FoodTruck.findById(req.params.id, (err, foodtruck) => {
    if (err) {
        res.send(err);
    }
    let newReview = new Review();

    newReview.title = req.body.title;
    newReview.text = req.body.text;
    newReview.foodtruck = foodtruck._id;
    newReview.save((err, review) => {
      if (err) {
        res.send(err);
      }
      foodtruck.reviews.push(newReview);
      foodtruck.save(err => {
        if (err){
          res.send(err);
        }
        res.json({message: 'Food truck review saved'})
      });
    });
  });
});

// get reviews for a newTruck
api.get('/reviews/:id', (req, res) => {
  Review.find({foodtruck: req.params.id}, (err, reviews) => {
    if (err){
      res.send(err);
    }
    res.json(reviews);
  });
});


  return api;
}
